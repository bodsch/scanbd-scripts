#!/bin/bash
#
# Copyright (c) 2019 - Bodo Schulz
#  Infrastructure Architect | Hacker-in-Residence
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; If not, see <http://www.gnu.org/licenses/>.

SCRIPTNAME=$(basename ${0} .sh)
SCRIPTDIRECTORY=$(dirname ${0})

VERSION="1.2.1"
VDATE="02.01.2019"

# ----------------------------------------------------------------------------------------

TIME=120
WATCH=
FOLDER=
SCRIPT=
SCRIPT_PARAMS=

. ${SCRIPTDIRECTORY}/helper.sh

# ----------------------------------------------------------------------------------------

version() {

  help_format_title="%-9s %s\n"

  echo ""
  printf  "$help_format_title" "watchdog"
  echo ""
  printf  "$help_format_title" " Version $VERSION ($VDATE)"
  echo ""
}

usage() {

  help_format_title="%-9s %s\n"
  help_format_desc="%-9s %-10s %s\n"
  help_format_example="%-9s %-30s %s\n"

  version

  printf \
    "$help_format_title" "Usage:" "$SCRIPTNAME [-h] [-v] [-T|--time] [-W|--watch] [-S|--script]"
}

# ----------------------------------------------------------------------------------------

watch_folder() {

  [[ -e ${WATCH} ]] || touch ${WATCH}

  while true
  do
    if [[ -f "${WATCH}" ]]
    then
      DATEC="$(date "+%Y%m%d%H%M%S")"
      DATEF="$(date "+%Y%m%d%H%M%S" -r ${WATCH})"

      diff=$(( (DATEC - DATEF) ))

      if [[ ${diff} -gt ${TIME} ]]
      then
        rm -f ${WATCH}
        to_logger "run script '${SCRIPT}' with params '${SCRIPT_PARAMS}'"
        exec ${SCRIPT} ${SCRIPT_PARAMS}

        exit 0
      fi
    fi

    sleep 10s
  done
}

# ----------------------------------------------------------------------------------------

# Parse parameters
while [ $# -gt 0 ]
do
  case "$1" in
    -h|--help) shift
      usage;
      exit 0
      ;;
    -v|--version) shift
      version;
      exit 0
      ;;
    -T|--time) shift
      TIME="${1}"
      ;;
    -W|--watch) shift
      WATCH=${1}
      ;;
    -S|--script) shift
      SCRIPT=${1}
      ;;
    --script-params) shift
      SCRIPT_PARAMS=${1}
      ;;
    *)  echo "Unknown argument: $1"
      exit $STATE_UNKNOWN
      ;;
  esac
shift
done

# ----------------------------------------------------------------------------------------

# Check that required argument (metric) was specified

[ -z "${TIME}" ] && {
  echo ""
  echo "Usage error: 'time' parameter missing"
  usage
  exit 1
}

[ -z "${WATCH}" ] && {
  echo ""
  echo "Usage error: 'watch' parameter missing"
  usage
  exit 1
} || {

  [ -f ${WATCH} ] || {
    echo ""
    echo "ERROR: file '${WATCH}' missing"
    usage
    exit 1
  }
}

[ -z "${SCRIPT}" ] && {
  echo ""
  echo "Usage error: 'script' parameter missing"
  usage
  exit 1
}|| {

  [ -e ${SCRIPT} ] || {
    echo ""
    echo "ERROR: script '${SCRIPT}' missing"
    usage
    exit 1
  }
}

watch_folder
