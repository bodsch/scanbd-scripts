#!/bin/bash
#
# Copyright (c) 2019 - Bodo Schulz
#  Infrastructure Architect | Hacker-in-Residence
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; If not, see <http://www.gnu.org/licenses/>.

SCRIPTNAME=$(basename ${0} .sh)
SCRIPTDIRECTORY=$(dirname ${0})

VERSION="1.2.1"
VDATE="02.01.2019"

FOLDER=
DESTINATION=

. ${SCRIPTDIRECTORY}/helper.sh

# ----------------------------------------------------------------------------------------

version() {

  help_format_title="%-9s %s\n"

  echo ""
  printf  "$help_format_title" "postprocess"
  echo ""
  printf  "$help_format_title" " Version $VERSION ($VDATE)"
  echo ""
}

usage() {

  help_format_title="%-9s %s\n"
  help_format_desc="%-9s %-10s %s\n"
  help_format_example="%-9s %-30s %s\n"

  version

  printf \
    "$help_format_title" "Usage:" "$SCRIPTNAME [-h] [-v] [-F|--folder] [-D|--destination]"
}

# ----------------------------------------------------------------------------------------

exec_unpaper() {

  local folder="${1}"
  local file="${2}"

  local input_file="${file}"
  local output_file="unpaper-${file}"

  [[ -f "${folder}/${input_file}" ]] || return

  to_logger "run unpaper for '${input_file}'"
  # Run unpaper on the images
  unpaper \
    --overwrite \
    --no-blurfilter \
    --no-grayfilter \
    --dpi 600 \
    "${folder}/${input_file}" \
    "${folder}/${output_file}"
}

exec_tesseract() {

  local folder="${1}"
  local file="${2}"

  local input_file="unpaper-${file}"
  local output_file="ocred-${file}"

  [[ -f "${folder}/${input_file}" ]] || return

  to_logger "run tesseract for '${input_file}'"
  tesseract \
    "${folder}/${input_file}" \
    "${folder}/${output_file}" \
    -l deu \
    pdf
}

exec_pdfunite() {

  local folder="${1}"
  local output_file="${2}"

  to_logger "join PDFs"

  if [ $(ls -1 "${folder}"/ocred-*.pdf | wc -l) -gt 0 ]
  then
    # Yes: Join PDFs
    pdfunite \
      "${folder}"/ocred-*.pdf \
      "${output_file}"
  fi

  to_logger "destination file: '${output_file}'"
}

run() {

  local postprocess_directory=${FOLDER}_postprocess_$$

  mv ${FOLDER} ${postprocess_directory}

  FOLDER=${postprocess_directory}

  to_logger "start scan postprocess"

  # Iterate through all pages
  for PAGE in $(ls "${FOLDER}"/page*)
  do
    FILE=$(basename "${PAGE}")

    # Determine standard deviation
    STDDEV=$(identify -verbose "${FOLDER}/${FILE}" | grep -i deviation | tail -n 1 | awk -F '(' ' { print $2 } ' | awk -F ')' ' { print $1 } ')
    # If standard deviation for all channels is too low, page is empty
    if [ $(echo ${STDDEV}'>'0.09 | bc -l) == "0" ]
    then
      to_logger "Page ${PAGE} empty, skipping"
      continue
    fi

    exec_unpaper ${FOLDER} ${FILE}
    exec_tesseract ${FOLDER} ${FILE}
  done

  exec_pdfunite ${FOLDER} ${DESTINATION}

  # clean up
  to_logger "clean up"
  rm -rf ${FOLDER}
}


# ----------------------------------------------------------------------------------------

# Parse parameters
while [ $# -gt 0 ]
do
  case "$1" in
    -h|--help) shift
      usage;
      exit 0
      ;;
    -v|--version) shift
      version;
      exit 0
      ;;
    -F|--folder) shift
      FOLDER="${1}"
      ;;
    -D|--destination) shift
      DESTINATION="${1}"
      ;;
    *)  echo "Unknown argument: $1"
      exit $STATE_UNKNOWN
      ;;
  esac
shift
done

# ----------------------------------------------------------------------------------------

[ -z "${FOLDER}" ] && {
  echo ""
  echo "Usage error: 'folder' parameter missing"
  usage
  exit 1
} || {

  [ -d ${FOLDER} ] || {
    echo ""
    echo "ERROR: folder '${FOLDER}' missing"
    usage
    exit 1
  }
}

[ -z "${DESTINATION}" ] && {
  echo ""
  echo "Usage error: 'destination' parameter missing"
  usage
  exit 1
}

run
