#!/bin/bash
#
# Copyright (c) 2019 - Bodo Schulz
#  Infrastructure Architect | Hacker-in-Residence
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# -- helper functions

to_logger() {

  local message=${1}
  logger \
    --tag "scanbd: ${SCRIPTNAME}" \
    "${message}"
}

watchdog() {

  touch ${LOCK_FILE}
  [[ -d ${TEMP_FOLDER} ]] || mkdir -p ${TEMP_FOLDER}

  if [[ $(ps ax | grep -v grep | grep watchdog.sh | wc -l) -gt 0 ]]
  then
    return
  fi

  to_logger "start watchdog"

  nohup ${SCRIPTDIRECTORY}/watchdog.sh \
    --time ${WATCHDOG_TIME} \
    --watch ${LOCK_FILE} \
    --script ${SCRIPTDIRECTORY}/postprocess.sh \
    --script-params "--folder ${temp_directory} --destination ${today_file}" &
}

