#!/bin/bash
#
# Copyright (c) 2019 - Bodo Schulz
#  Infrastructure Architect | Hacker-in-Residence
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; If not, see <http://www.gnu.org/licenses/>.
#
#
# look in scanbd.conf for environment variables

SCRIPTNAME=$(basename ${0} .sh)
SCRIPTDIRECTORY=$(dirname ${0})

ENV_SCRIPT="/etc/scanbd/scanbd.env"

if [[ ! -e ${ENV_SCRIPT} ]]
then
  error="can find environment file!"
  logger -t "scanbd: ${SCRIPTNAME}" "ERROR ${error}"
  echo "ERROR ${error}"

  exit 1
fi

. ${ENV_SCRIPT}
. ${SCRIPTDIRECTORY}/helper.sh

# printout all env-variables
/usr/bin/printenv > /tmp/scanbd.script.${SCANBD_ACTION}

# -------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------

time_stamp=$(date +%Y%m%d%H%M)
current_date=$(date +%Y%m%d)
temp_directory=/tmp/${current_date}

today_file="${DESTINATION_DIRECTORY}/$(date +%Y%m%d_%H%M).pdf"

# -------------------------------------------------------------------------------------------------

to_logger "Begin of ${SCANBD_ACTION} for device ${SCANBD_DEVICE}"

watchdog

set -e

scanimage \
  --formatted-device-list \
  " scanner number %i device %d is a %t, model %m, produced by %v "

[[ -d ${temp_directory} ]] || mkdir -p ${temp_directory}

count=01

LAST=$(ls ${temp_directory} | sort -n | tail -1 | sed 's/page_\([0-9]\+\).*/\1/g')

if [[ ! -z ${LAST} ]]
then
  count=$(printf "%02d" $(( 1 + $LAST )))
fi

to_logger "scan in file: ${temp_directory}/page_${count}.${SCAN_FORMAT}"

#  --batch=${temp_directory}/page_${count}.${SCAN_FORMAT} \
#  --batch-count=1 \

scanimage \
  --verbose \
  --resolution=${SCAN_RESOLUTION} \
  --mode=${SCAN_MODE} \
  --format=${SCAN_FORMAT} \
  --depth=${SCAN_COLOR_DEPTH} \
  > ${temp_directory}/page_${count}.${SCAN_FORMAT}

to_logger "scan was succesfull" "page_${count}.${SCAN_FORMAT}"

to_logger "End   of ${SCANBD_ACTION} for device ${SCANBD_DEVICE}"

## -------------------------------------------------------------------------------------------------
