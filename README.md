# scanbd-scripts

Set of scripts they usful with [scanbd](https://sourceforge.net/projects/scanbd).

They implents an watchdog to start postprocessing task to create an multipage PDF.


## requirements

- scanbd
- sane
- bc
- unpaper
- poppler-utils
- tesseract-ocr
- tesseract-ocr-deu
- tesseract-ocr-deu-frak
- tesseract-ocr-eng
- mutt
- ssmtp (or similar)


## scripts

`watchdog.sh`

`postprocess.sh`

`helper.sh`

`scan.sh`

`email.sh`

